import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as copyToClipboard from 'copy-to-clipboard';
import { trigger, transition, style, animate } from '@angular/animations';

interface ShorteningResult {
  id: number;
  shortened_url: string;
  error?: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('slideIn', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('500ms ease-in', style({transform: 'translateY(0%)'}))
      ])
    ])
  ]
})
export class AppComponent {
  public copy = copyToClipboard;

  public shortening = false;
  public urls: Array<string> = [ ];
  public error: string;

  constructor(private http: HttpClient) { }

  public shorten(url: HTMLInputElement) {
    this.shortening = true;

    this.http.post('/new', { url_to_shorten: url.value }).subscribe((shorteningResult: ShorteningResult) => {
      this.shortening = false;

      if (shorteningResult.id) {
        this.urls.push(shorteningResult.shortened_url);
        this.error = url.value = '';
      } else {
        this.error = shorteningResult.error;
      }
    });
  }
}
