FROM node:latest

# Install application dependencies
RUN npm install -g @angular/cli ts-node typescript

# Install server dependencies
WORKDIR /app
COPY package*.json ./
RUN npm install

# Install client dependencies
WORKDIR /app/ui
COPY ui/package*.json ./
RUN npm install

# Copy source files
WORKDIR /app
COPY . .

# Build client application
WORKDIR /app/ui
RUN npm run build

# Start application
WORKDIR /app
CMD ts-node src/server.ts
