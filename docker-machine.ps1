# Remove previous AWS docker machine
docker-machine rm aws -f

# Create docker machine on AWS
docker-machine create `
  --driver amazonec2 `
  --amazonec2-region us-west-1 `
  --amazonec2-instance-type t3.small `
  --amazonec2-open-port 80 `
  aws

# Switch docker environment to AWS
docker-machine env aws | Invoke-Expression

# Run application on AWS
docker-compose up --detach
