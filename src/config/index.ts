require('dotenv').config();

export const

  DOMAIN_NAME = 'abbrv.ca',
  MINIMUM_URL_LENGTH = 3,

  CHARSET = 'xNPlf16O3FuHb2tWm8DGZsSp4EqA5QzaYckdeVCX79TgMLBIorRivnKJh0yUwj',

  PGSQL_TABLE = 'urls',
  PGSQL_CONNECTION = {
    host: 'pg',
    port: 5432,
    user: 'postgres',
    database: 'postgres',
    password: process.env.POSTGRES_PASSWORD
  }

;
