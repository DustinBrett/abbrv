import * as express from 'express';
import { Application, Request, Response } from 'express';
import * as cors from 'cors';
import { json } from 'body-parser';
import * as compression from 'compression';
import controllers from '../controllers';
import { DOMAIN_NAME } from '../config';

export default class ApiService {
  private express: Application;

  constructor() {
    this.express = express();

    this.useRequestMiddleware();
    this.useSecureHttpHeaders();
    this.useControllers();
  }

  private useRequestMiddleware(): void {
    this.express.use(cors({ methods: ['GET', 'POST'] }));
    this.express.use(json());
    this.express.use(compression());
  }

  private useSecureHttpHeaders(): void {
    this.express.disable('x-powered-by');
  }

  private useControllers(): void {
    this.express.use(controllers());
    this.express.use((req: Request, res: Response) => res.sendStatus(404));
  }

  public listen(port: number = 80): void {
    this.express.listen(port, () => {
      console.log(`ApiService: Listening @ http://${ DOMAIN_NAME }:${ port }/`);
    });
  }
}
