import { CHARSET } from '../config';

export default class EncodingService {
  private base: number = CHARSET.length;

  public encode(num: number): string {
    let str = '';

    while (num > 0) {
      str += CHARSET[num % this.base];
      num = Math.floor(num / this.base);
    }

    return str.split('').reverse().join('');
  }

  public decode(str: string): number {
    let num = 0;

    for (let i = 0; i < str.length; i++) {
      num += CHARSET.indexOf(str[i]) * Math.pow(this.base, str.length - i - 1);
    }

    return num;
  }
}
