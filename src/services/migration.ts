import { db } from '../services/db';
import { MINIMUM_URL_LENGTH, PGSQL_TABLE, CHARSET } from '../config';

const UNDEFINED_TABLE = '42P01'; // https://www.postgresql.org/docs/8.2/errcodes-appendix.html

export default class MigrationService {
  private async createTable(): Promise<void> {
    const createTable = `
      CREATE TABLE ${ PGSQL_TABLE }
      (
        id SERIAL UNIQUE,
        url TEXT,
        shortened TEXT,
        created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
      )
    `;
    const minimumId = Math.pow(CHARSET.length, MINIMUM_URL_LENGTH - 1);
    const alterTable = `ALTER SEQUENCE ${ PGSQL_TABLE }_id_seq RESTART ${ minimumId }`;

    await db.transaction(async () => {
      await db.query(createTable);
      await db.query(alterTable);
    });

    console.log('MigrationService: Schema created.');
  }

  public async up(): Promise<void> {
    try {
      const { rows } = await db.query(`SELECT * FROM ${ PGSQL_TABLE } LIMIT 1`);

      if (rows.length >= 0) {
        console.log('MigrationService: Schema already exists, migration not needed.');
      }
    } catch (ex) {
      if (ex.code === UNDEFINED_TABLE) {
        this.createTable();
      } else {
        console.error('MigrationService: Error during migration.');
      }
    }
  }
}
