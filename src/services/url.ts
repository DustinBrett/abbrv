import Encoding from '../services/encoding';
import { DOMAIN_NAME, PGSQL_TABLE } from '../config';
import { db } from '../services/db';
import { parse } from 'url';

export default class UrlService {
  private encoding = new Encoding();

  private check(url: string): boolean {
    const parsedUrl = parse(url);
    const hasHost = parsedUrl.host !== null;
    const isHttpProtocol = parsedUrl.protocol === 'http:' || parsedUrl.protocol === 'https:';

    return (hasHost && isHttpProtocol);
  }

  public async shorten(url: string): Promise<any> {
    if (this.check(url)) {
      return db.transaction(async () => {
        const { rows } = await db.query(`INSERT INTO ${ PGSQL_TABLE } (url) VALUES ($1) RETURNING id`, [ url ]);
        const { id } = rows[0];
        const shortenedUrl = this.encoding.encode(id);

        await db.query(`UPDATE ${ PGSQL_TABLE } SET shortened = $1 WHERE id = $2`, [ shortenedUrl, id ]);

        return { id, shortened_url: `http://${ DOMAIN_NAME }/${ shortenedUrl }` };
      });
    } else {
      return { error: 'Url does not meet requirements.' };
    }
  }

  public async expand(shortenedUrl: string): Promise<any> {
    try {
      const id = this.encoding.decode(shortenedUrl);
      const { rows } = await db.query(`SELECT url FROM ${ PGSQL_TABLE } WHERE id = $1`, [ id ]);

      return {
        id,
        shortened_url: `http://${ DOMAIN_NAME }/${ shortenedUrl }`,
        original_url: rows.length > 0 ? rows[0].url : null
      };
    } catch (ex) {
      return { error: 'Error retrieving url.' };
    }
  }
}
