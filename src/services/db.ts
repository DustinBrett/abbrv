import { Client, ClientConfig } from 'pg';
import { PGSQL_CONNECTION } from '../config';

class DbService {
  private client: Client;

  constructor(config: ClientConfig) {
    this.client = new Client(config);
  }

  public connect() {
    return this.client.connect();
  }

  public query(text: string, values?: any[]) {
    return this.client.query(text, values);
  }

  public async transaction(transactionFunction: Function) {
    try {
      await this.client.query('BEGIN');
      const transactionResults = await transactionFunction();
      await this.client.query('COMMIT');

      return transactionResults;
    } catch (ex) {
      await this.client.query('ROLLBACK');
    }
  }
}

export const db = new DbService(PGSQL_CONNECTION);
