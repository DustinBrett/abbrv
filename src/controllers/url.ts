import { Router, Request, Response, static as staticFiles } from 'express';
import { join } from 'path';
import UrlService from '../services/url';

export default {

  path: '/',

  handler: ((): Router => {
    const url = new UrlService();

    return Router()

      .use(staticFiles(join(__dirname, '../../ui/dist')))

      .post('/new', async (req: Request, res: Response) => {
        res.json(
          req.body.url_to_shorten
          ? await url.shorten(req.body.url_to_shorten)
          : { error: 'Required field missing: url_to_shorten' }
        );
      })

      .get('/:url', async (req: Request, res: Response) => {
        const { original_url } = await url.expand(req.params.url);

        if (original_url) {
          res.redirect(301, original_url);
        } else {
          res.json({ error: 'Url not found.' });
        }
      })

      .get('/urls/:url', async (req: Request, res: Response) => {
        const data = await url.expand(req.params.url);

        res.json(data.original_url ? data : { error: 'Url not found.' });
      })

    ;

  })()

};
