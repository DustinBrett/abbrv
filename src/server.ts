import { db } from './services/db';
import MigrationService from './services/migration';
import ApiService from './services/api';

db.connect().then(() => {
  new MigrationService().up();
  new ApiService().listen();
});
